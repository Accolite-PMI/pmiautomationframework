package pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import common.AppConstants;
import common.PageBase;

public class LoginPage extends PageBase{

	public LoginPage(){
	}
	
	public void login(){
		driver.get(AppConstants.url);
		driver.findElement(By.xpath("//input[@formcontrolname='username']")).clear();
		driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys(AppConstants.adminUserName);
		driver.findElement(By.xpath("//input[@formcontrolname='password']")).clear();
		driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys(AppConstants.adminPassword);
		driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
		waitForPageLoad();
		Assert.assertTrue(driver.getTitle().equals("Metrics Enterprise"),"Unable to Login");
	}
	
	public HomePage webEditorLogin(){
		driver.get(AppConstants.url);
		doLogin(AppConstants.webEditorUserName, AppConstants.webEditorPassword);
		return new HomePage();
	}
	
	public HomePage customUserLogin(String userName,String password){
		doLogin(userName, password);
		return new HomePage();
	}
	
	public HomePage adminLogin(){
		driver.get(AppConstants.url);
		doLogin(AppConstants.adminUserName, AppConstants.adminPassword);
		waitForPageLoad();
		System.out.println(driver.getTitle());
		//Assert.assertTrue(driver.getTitle().equals("Protein Metrics"),"Unable to Login");
		return new HomePage();
	}
	
	public void doLogin(String userName,String password){
		driver.findElement(By.xpath("//input[@formcontrolname='username']")).clear();
		driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys(userName);
		driver.findElement(By.xpath("//input[@formcontrolname='password']")).clear();
		driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys(password);
		driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
	}
}
