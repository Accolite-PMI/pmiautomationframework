package pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import common.PageBase;
import common.Util;

public class FolderPage extends PageBase{
	
	boolean optionExists = false;
	
	public FolderPage(){
	}
	
	public void selectMyFilesMenuItem(){
		Util.sleep(5000);
		driver.findElement(By.xpath("(//label[@title='My Files']/parent::span)[1]")).click();
	}
	
	public void getCreateFolderDialolg(){
		//Actions actions = new Actions(driver);
		Util.sleep(5000);
		WebElement element = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span)[1]"));
		//actions.contextClick(element).perform();
		rightClick(element);
		driver.findElement(By.xpath("//span[contains(text(),'Create New Folder')]")).click();
	}
	
	public void createFolder(String folderName){
		driver.findElement(By.xpath("//b[contains(text(),'Name:')]/parent::span/parent::div/following-sibling::div[1]/input")).sendKeys(folderName);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Util.sleep(1000);
	}
	
//	public void selectCreatedFolder(String folderName) throws InterruptedException{
//		//Util.sleep(1000);
//		//driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
//		Util.sleep(1000);
//		WebElement element1 = driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')])[1]"));
//		JavascriptExecutor js = (JavascriptExecutor)driver;
//		js.executeScript("arguments[0].click();", element1);
//	}
	
	public void selectCreatedFolder(String folderName) throws InterruptedException{
		Util.sleep(1000);
		String notExpanded = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]/parent::div")).getAttribute("aria-expanded");
		if(notExpanded.equalsIgnoreCase("false"))
		{
			driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
			
		}
		Util.sleep(1000);
		WebElement element1 = driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')])[1]"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", element1);
	}
	
	public void createSubFolder(String subFolderName,String folderName) throws InterruptedException{
		driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		Thread.sleep(1000);
		WebElement folder = driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')])[1]"));
		rightClick(folder);
		driver.findElement(By.xpath("//span[contains(text(),'Create New Folder')]")).click();
		driver.findElement(By.xpath("//b[contains(text(),'Name:')]/parent::span/parent::div/following-sibling::div[1]/input")).sendKeys(subFolderName);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Util.sleep(1000);	
	}
	
	public void searchFolder(String folderName){
		driver.findElement(By.xpath("//input[@name='search']")).sendKeys(folderName);
		
	}
	
	
	
	public void allOptions()
	{
		optionExists = driver.findElement(By.xpath("(//label[@title='Team Files']/parent::span)[1]")).isDisplayed();
	    assertEquals(true, optionExists);
	    optionExists = driver.findElement(By.xpath("(//label[@title='Shared With Me']/parent::span)[1]")).isDisplayed();
		assertEquals(true, optionExists);
		optionExists = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span)[1]")).isDisplayed();
		assertEquals(true, optionExists);
		optionExists = driver.findElement(By.xpath("(//label[@title='Deleted Items']/parent::span)[1]")).isDisplayed();
		assertEquals(true, optionExists);
	}
	
	//Action method to end file sharing
	public void endSharing(String folderName)
	{
		
		WebElement sharedFolder = driver.findElement(By.xpath("//tbody[@class='ui-table-tbody']//label[contains(text(),'"+folderName+"')]"));
		Assert.assertEquals(sharedFolder.getText(), folderName);
		sharedFolder.click();
		rightClick(sharedFolder);
		driver.findElement(By.xpath("//span[contains(text(),'End Sharing')]/parent::a[@class='ui-menuitem-link ui-corner-all ng-star-inserted']")).click();
		System.out.println("Filesharing ended");
		
	}
	
	//Action method to check the Shared Folder
	public boolean checkSharedFolder(String folderName)
	{
		
		boolean flag = driver.findElement(By.xpath("//label[contains(text(),'"+folderName+"')]")).isDisplayed();
		return flag;
		
	}
	
	//Action method to check DeleteButton Status
	public boolean checkSharedFolderDeleteButtonStatus(String folderName)
	{
		
		WebElement sharedFolder = driver.findElement(By.xpath("//tbody[@class='ui-table-tbody']//label[contains(text(),'"+folderName+"')]"));
		Assert.assertEquals(sharedFolder.getText(), folderName);
		sharedFolder.click();
		rightClick(sharedFolder);
		//WebElement share = driver.findElement(By.xpath("//span[contains(text(),'Delete')]/parent::a[@class='ui-menuitem-link ui-corner-all ng-star-inserted']"));
		WebElement share = driver.findElement(By.xpath("//span[contains(text(),'Delete')]"));
		if(share.isEnabled())
			return false;
		else
			return true;
		
	}
	
	public void clickonSearch()
	{
		Util.sleep(500);
		WebElement element = driver.findElement(By.xpath("//span[@id='basic-text1']/i"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
	}
	
	public void matchFileName(String folderName)
	{
		waitForPageLoad();
		String createdFolder = driver.findElement(By.xpath("//tr[@class='ui-selectable-row ng-star-inserted']/td[2]")).getText();
		Assert.assertEquals(createdFolder, folderName);
		
	}
	
	public void treeExpanded()
	{
		String notExpanded = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]/parent::div")).getAttribute("aria-expanded");
		Assert.assertEquals("false", notExpanded);
		driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		String expanded = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]/parent::div")).getAttribute("aria-expanded");
		Assert.assertEquals("true", expanded);
	}
	
	public void toolTip()
	{
	WebElement firstFolder = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]/parent::div/parent::li/ul/p-treenode[1]/li//label"));
	String folderName = driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]/parent::div/parent::li/ul/p-treenode[1]/li//label")).getText();
	Actions action = new Actions(driver);
	action.moveToElement(firstFolder).perform();
	String toolTipText = firstFolder.getText();
	Assert.assertEquals(folderName, toolTipText);
	
	}

	
	public void uploadFile(){
		Util.sleep(5000);
		WebElement element = driver.findElement(By.xpath("(//button[contains(text(),' New ')])[1]/following-sibling::button"));
		new Actions(driver).doubleClick(element).build().perform();
		//driver.findElement(By.xpath("//a[contains(text(),'New Upload') and @class='dropdown-item pointer']")).sendKeys("C:\\Users\\Sandeep\\Downloads\\cancelreport.blgc");		
	}
	
	public void uploadFile(String subFolderName,String folderName) throws IOException{
		//driver.findElement(By.xpath("(//label[@title='My Files']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		//Util.sleep(1000);
		System.out.println("Uploading File....");
		driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')]/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		autoItUpload();
		Util.sleep(3000);
		driver.findElement(By.xpath("(//label[contains(text(),'"+subFolderName+"')])[1]")).click();
		autoItUpload();
	}
	
	public void autoItUpload() throws IOException
	{
		Util.sleep(5000);
		WebElement element = driver.findElement(By.xpath("(//button[contains(text(),' New ')])[1]/following-sibling::button"));
		new Actions(driver).doubleClick(element).build().perform();
		WebElement newUpload = driver.findElement(By.xpath("(//a[contains(text(),' New Upload')])[2]"));
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", newUpload);
		Util.sleep(5000);
	    Runtime.getRuntime().exec("C:\\Work\\workspace\\PMI\\FileUpload.exe");
	    System.out.println("Uploaded File successfully....");
	    Util.sleep(5000);
	}
	
	public void ClickNewDropDown(){
		Util.sleep(10000);
		WebElement  element= driver.findElement(By.xpath("(//button[@data-toggle='dropdown'])[1]"));
		new Actions(driver).moveToElement(element).build().perform();
		//WebElement element =driver.findElement(By.xpath("(//span[contains(text(),'Toggle Dropdown')])[1]"));
		//JavascriptExecutor js=(JavascriptExecutor)driver;
		//js.executeScript("arguments[0].click();", element);
	}
	
	public void shareFolderToAnotherUser(String userName) throws InterruptedException{
		WebElement shareLink = driver.findElement(By.xpath("//button[@title='Share']"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", shareLink);
		driver.findElement(By.xpath("//input[@placeholder='Select User']")).sendKeys(userName);
		Thread.sleep(100);
		driver.findElement(By.xpath("//li[contains(text(),'"+userName+" ')]")).click();
		//Select dropDown = new Select(driver.findElement(By.id("186_User")));
		//dropDown.selectByVisibleText("Can Share ");
		WebElement permissionDropDown = driver.findElement(By.xpath("//select[@class='form-dropdown']"));
		selectByVisibleText(permissionDropDown, "Can Share ");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

	public LoginPage logoutCurrentUser(){
		WebElement signOut = driver.findElement(By.xpath("//div[@class='btn-group']/a"));
		JavascriptExecutor js1 = (JavascriptExecutor)driver;
		js1.executeScript("arguments[0].click();", signOut);
		WebElement signoutClick = driver.findElement(By.xpath("//a[contains(text(),'Sign Out')]"));
		JavascriptExecutor js2  = (JavascriptExecutor)driver;
		js2.executeScript("arguments[0].click();", signoutClick);
		return new LoginPage();
	}
	
	public void selectSharedWithMenuItem(){
		WebElement element= driver.findElement(By.xpath("(//label[@title='Shared With Me']/parent::span)[1]"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", element);
	}
	
	
	
	public void validateSharedFolder(String folderName) throws Exception{
		WebElement sharedFolder = driver.findElement(By.xpath("//tbody[@class='ui-table-tbody']//label[contains(text(),'"+folderName+"')]"));
		Assert.assertEquals(sharedFolder.getText(), folderName);
		sharedFolder.click();
		//Actions actions = new Actions(driver);
		//actions.contextClick(sharedFolder).perform();
		rightClick(sharedFolder);
		WebElement share = driver.findElement(By.xpath("//span[contains(text(),'Share')]/parent::a[@class='ui-menuitem-link ui-corner-all ng-star-inserted']"));
		if(share.isEnabled())
			System.out.println("Element is enabled");
		else
			throw new Exception("Element is not enabled");
	}
	
	public void validateDisabledOption(String folderName) throws Exception{
		Util.sleep(1000);
		driver.findElement(By.xpath("(//label[@title='Shared With Me']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		Thread.sleep(1000);
		WebElement sharedFolder = driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')])[1]"));
		rightClick(sharedFolder);
		
		//Check if options are disabled after sharing the folder
		String renameMenuItem = driver.findElement(By.xpath("//span[contains(text(),'Rename')]/parent::a")).getAttribute("class");
		boolean menuItemState = renameMenuItem.contains("disabled");
		Assert.assertTrue(menuItemState);
		String moveMenuItem = driver.findElement(By.xpath("//span[contains(text(),'Move')]/parent::a")).getAttribute("class");
		boolean menuMoveState = moveMenuItem.contains("disabled");
		Assert.assertTrue(menuMoveState);
	
	}
	
	public void verifyMoveOption(String folderName,String subFolderName,String tempfolderName) throws Exception
	{
		//driver.findElement(By.xpath("(//label[@title='Shared With Me']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')])[1]")).click();
		Util.sleep(4000);
		driver.findElement(By.xpath("(//label[contains(text(),'"+folderName+"')]/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		Util.sleep(1000);
		WebElement subFolder = driver.findElement(By.xpath("(//label[contains(text(),'Shared With Me')]/parent::span/parent::span/parent::div/following-sibling::ul//label[contains(text(),'"+subFolderName+"')])[1]"));
		//WebElement subFolder = driver.findElement(By.xpath("(//label[contains(text(),'"+subFolderName+"')])[1]"));
		rightClick(subFolder);
		
		driver.findElement(By.xpath("//span[contains(text(),'Move')]/parent::a")).click();
		driver.findElement(By.xpath("//h4[contains(text(),'Move Folder')]/parent::div/following-sibling::div//label[contains(text(),'"+tempfolderName+"')]")).click();
		Util.sleep(1000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.navigate().refresh();
		
		driver.findElement(By.xpath("(//label[@title='Shared With Me']/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		Thread.sleep(100);
		driver.findElement(By.xpath("(//label[contains(text(),'"+tempfolderName+"')]/parent::span/parent::span/preceding-sibling::span)[1]")).click();
		boolean subfolderExists = driver.findElement(By.xpath("(//label[contains(text(),'"+subFolderName+"')])[1]")).isDisplayed();
		Assert.assertTrue(subfolderExists);
		
	}
	
	public void validateNewDropDownOptions(){
		driver.findElement(By.xpath("(//span[contains(text(),'Toggle Dropdown')]/parent::button)[1]")).click();
		
	}
	
}
