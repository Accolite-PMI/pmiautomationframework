package pages;

import org.openqa.selenium.By;
import common.PageBase;

public class HomePage extends PageBase {
	
	public HomePage(){
	}
	
	public FolderPage clickOnFiles(){
		driver.findElement(By.xpath("//label[contains(text(),'Files')]/parent::div/preceding-sibling::app-hover-image/img")).click();
		waitForPageLoad();
		return new FolderPage();
	}
	
	public boolean checkProtienMetricsIcon() 
	{		
		return driver.findElement(By.xpath("//img[@class='logo']")).isDisplayed();
	}
	
	public boolean checkConfigurationTab() 
	{		
		return driver.findElement(By.xpath("//span[contains(text(),'Configuration')]")).isDisplayed();
	}

	public boolean checkUsersTab() 
	{		
		return driver.findElement(By.xpath("//span[contains(text(),'Users')]")).isDisplayed();
	}
	
	public boolean checkGroupsTab() 
	{		
		return driver.findElement(By.xpath("//span[contains(text(),'Groups')]")).isDisplayed();
	}
	
	public boolean checkDeletionsTab() 
	{		
		return driver.findElement(By.xpath("//span[contains(text(),'Deleted')]")).isDisplayed();
	}
	
	public void moveToConfiguration() 
	{
		driver.findElement(By.xpath("//span[contains(text(),'Configuration')]")).click();
	}
	
	
	
	
}
