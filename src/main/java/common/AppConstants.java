package common;

import java.util.Properties;

public class AppConstants {
	
	

	

	public static String url = null;
	public static String adminUserName;
	public static String adminPassword;
	public static String webEditorUserName;
	public static String webEditorPassword;
	static Properties configProperties = null;
	//static Properties testProperties = null;
	//static User user = new User();

	static {
		configProperties = Util.loadPropertiesFile(Constants.CONFIG_DIR
				+ "/config.properties");
		initTestVars();
	}

	static void initTestVars() {
		url = configProperties.getProperty("baseURL");
		adminUserName = configProperties.getProperty("adminUsername");
		adminPassword = configProperties.getProperty("adminPassword");
		webEditorUserName = configProperties.getProperty("webEditorUsername");
		webEditorPassword = configProperties.getProperty("webEditorPassword");
	}


}
