package common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

public final class Util {
	public static Properties loadPropertiesFile(String filename) {
		Properties properties = new Properties();
		FileInputStream inputStream = null;

		try {
			inputStream = new FileInputStream(filename);
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			// failTest("Error while loading properties from '" + filename +
			// "': " + e.getMessage());
			TestBase.failTest("Error while loading properties from '" + filename + "': " + e.getMessage());
		} catch (IOException e) {
			// failTest("Error while loading properties from '" + filename +
			// "': " + e.getMessage());
			TestBase.failTest("Error while loading properties from '" + filename + "': " + e.getMessage());
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// failTest("Error in finally block while loading properties from '"
					// + filename + "': " + e.getMessage());
					TestBase.failTest("Error in finally block while loading properties from '" + filename + "': "
							+ e.getMessage());
				}
			}
		}

		return properties;
	}

	public static void printTestFailedMessage(String prefix, String message) {
		System.out.println("\n\n" + prefix);
		System.out.println("\t" + message + "\n\n");
	}

	public static void printMessage(String message) {
		System.out.println("*** " + getTimestamp(DateFormat.MEDIUM, DateFormat.LONG) + " *** " + message);
	}

	public static void printInfo(String message) {
		if (!message.isEmpty()) {
			printMessage("INFO: " + message);
		} else {
			System.out.println(message);
		}
	}

	public static void printWarning(String message) {
		printMessage("WARNING: " + message);
	}

	public static void printError(String message) {
		printMessage("ERROR: " + message);
	}

	public static String getTimestamp(int dateFormat, int timeFormat) {
		return getCurrentDateTime(dateFormat, timeFormat);
	}

	public static String getCurrentDateTime(int dateFormat, int timeFormat) {
		return DateFormat.getDateTimeInstance(dateFormat, timeFormat).format(new Date());
	}

	public static String getCurrentDate(String outputFormat) {
		DateFormat dformat = new SimpleDateFormat(outputFormat);
		return dformat.format(new Date());
	}

	public static String getCurrentDate() {
		return getCurrentDate(Constants.DEFAULT_DATE_FORMAT);
	}

	public static String getCurrentDate(int dateFormat) {
		return DateFormat.getDateInstance(dateFormat).format(new Date());
	}

	// ASCII values instead normal characters
	public static String getRandomString(int length) {
		String charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = getRandomNumber(0, charSet.length() - 1);
			char ch = charSet.charAt(number);
			randStr.append(ch);
		}
		return randStr.toString();
	}

	public static int getRandomNumber(int minimum, int maximum) {
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(maximum);
		return minimum + randomInt;
	}

	public static void sleep(long millis) {
		if (millis > 0) {
			try {
				Thread.sleep(millis);
			} catch (InterruptedException ie) {
				throw new RuntimeException(
						"An InterruptedException (" + ie.getMessage() + ") occurred while in Thread.sleep()");
			}
		}
	}

	/*public static HashMap<String, String> generateCreateOrderData(String[] dataFields) {
		TestDataGenerator generator = new TestDataGenerator();
		return generator.getCreateOrderData(dataFields);
	}*/

	public static String executeShellCommand(String command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}
}
