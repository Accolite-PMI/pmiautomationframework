package common;

public class Constants {
	public static final String TEST_PASSED_MESSAGE_TEXT			= "TEST PASSED";
	public static final String TEST_FAILED_MESSAGE_TEXT			= "TEST FAILED";
	public static final String TEST_FAILED_MESSAGE_PREFIX		= TEST_FAILED_MESSAGE_TEXT + "! Reason:  ";
	public static final String BASE_DIR							= "src/main";
	public static final String TEST_DIR							= "src/test";
	public static final String RESOURCE_DIR						= BASE_DIR + "/resources/drivers";
	public static final String TESTDATA_DIR						= TEST_DIR + "/testdata"; 
	public static final String CONFIG_DIR						= BASE_DIR + "/config";	
	public static final String DEFAULT_DATE_FORMAT				= "MM-dd-yyyy";
	public static final int DEFAULT_PAGEELEMENT_TIMEOUT_SECOND	= 20; 
}
