package common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.Assert;

import pages.Pages;

public class TestBase {
	public static WebDriver driver = null;
	public Pages pages = null;
	private String launchDriver = null;

	protected TestBase() {
		launchDriver = System.getProperty("appBrowser");
		if (launchDriver == null)
			driverSetup("chrome", false);
		else
			driverSetup(launchDriver, false);
		pages = new Pages();
	}



	public void driverSetup(String launchDriver, boolean withDriverBinary) {
		if (launchDriver.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		}  else if (launchDriver.equalsIgnoreCase("chrome")) {
			if (System.getProperties().toString().contains("Windows")) {
				System.setProperty("webdriver.chrome.driver",
						Constants.RESOURCE_DIR + "/chromedriver.exe");
			}
			ChromeOptions chromeOpt = new ChromeOptions();
			chromeOpt.addArguments("test-type");
			chromeOpt.addArguments("allow-running-insecure-content");
			chromeOpt.addArguments("--disable-popup-blocking");
			driver = new ChromeDriver(chromeOpt);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} else if (launchDriver.equalsIgnoreCase("ie")
				|| launchDriver.equalsIgnoreCase("Internet Explorer")) {
			System.setProperty("webdriver.ie.driver",
					Constants.RESOURCE_DIR + "/IEDriverServer.exe");
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.IGNORE);
			options.destructivelyEnsureCleanSession();
			options.ignoreZoomSettings();
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new InternetExplorerDriver(options);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}
	public static void failTest(String message) {
		Assert.fail(message);
	}
}
