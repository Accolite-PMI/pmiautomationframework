package workflow;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import common.TestBase;
import pages.HomePage;


public class ET69_VerifyConfigurationTabAsAdmin extends TestBase{
	
	HomePage hPage;
	SoftAssert softAssert = new SoftAssert();
	
	public ET69_VerifyConfigurationTabAsAdmin() 
	{		
		super();
	}
	
	//@BeforeMethod
	@BeforeTest
	public void init() 
	{
		System.out.println("Intitializing....");
		hPage = pages.login().adminLogin();		
	}
	
	@Test
	public void verifyOptions() 
	{
		System.out.println("Start Test1..");
		hPage.moveToConfiguration();
		
		boolean flag1 = driver.findElement(By.xpath("//td[contains(text(),'DaysAfterAutoArchiveDeletedItem')]")).isDisplayed();
		softAssert.assertTrue(flag1, "'DaysAfterAutoArchiveDeletedItem' is NOT showing in Configuration");
		
		boolean flag2 = driver.findElement(By.xpath("//td[contains(text(),'PartialFileCleanupHours')]")).isDisplayed();
		softAssert.assertTrue(flag2, "'PartialFileCleanupHours' is NOT showing in Configuration");
		
		boolean flag3 = driver.findElement(By.xpath("//td[contains(text(),'RemoveOrphanHours')]")).isDisplayed();
		softAssert.assertTrue(flag3, "'RemoveOrphanHours' is NOT showing in Configuration");
		
		boolean flag4 = driver.findElement(By.xpath("//td[contains(text(),'SessionTimeout')]")).isDisplayed();
		softAssert.assertTrue(flag4, "'SessionTimeout' is NOT showing in Configuration");
		
		boolean flag5 = driver.findElement(By.xpath("//td[contains(text(),'LockoutEmail')]")).isDisplayed();
		softAssert.assertTrue(flag5, "'LockoutEmail' is NOT showing in Configuration");
		
		boolean flag6 = driver.findElement(By.xpath("//td[contains(text(),'MaxSignInAttempts')]")).isDisplayed();
		softAssert.assertTrue(flag6, "'MaxSignInAttempts' is NOT showing in Configuration");
		
		boolean flag7 = driver.findElement(By.xpath("//td[contains(text(),'AllowDeletion')]")).isDisplayed();
		softAssert.assertTrue(flag7, "'AllowDeletion' is NOT showing in Configuration");
		
		softAssert.assertAll();
		System.out.println("End Test..");
		
	}
	
	@Test
	public void verifyEditForEachOption() 
	{		
		System.out.println("Start Test2..");
		hPage.moveToConfiguration();
		
		boolean flag1 = driver.findElement(By.xpath("//td[contains(text(),'DaysAfterAutoArchiveDeletedItem')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag1, "EDIT options for 'DaysAfterAutoArchiveDeletedItem' is NOT showing in Configuration");
		
		boolean flag2 = driver.findElement(By.xpath("//td[contains(text(),'PartialFileCleanupHours')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag2, "EDIT options for 'PartialFileCleanupHours' is NOT showing in Configuration");
		
		boolean flag3 = driver.findElement(By.xpath("//td[contains(text(),'RemoveOrphanHours')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag3, "EDIT options for 'RemoveOrphanHours' is NOT showing in Configuration");
		
		boolean flag4 = driver.findElement(By.xpath("//td[contains(text(),'SessionTimeout')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag4, "EDIT options for 'SessionTimeout' is NOT showing in Configuration");
		
		boolean flag5 = driver.findElement(By.xpath("//td[contains(text(),'LockoutEmail')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag5, "EDIT options for 'LockoutEmail' is NOT showing in Configuration");
		
		boolean flag6 = driver.findElement(By.xpath("//td[contains(text(),'MaxSignInAttempts')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag6, "EDIT options for 'MaxSignInAttempts' is NOT showing in Configuration");
		
		boolean flag7 = driver.findElement(By.xpath("//td[contains(text(),'AllowDeletion')]//following-sibling::td//button")).isDisplayed();
		softAssert.assertTrue(flag7, "EDIT options for 'AllowDeletion' is NOT showing in Configuration");
		
		softAssert.assertAll();
	}
	
	
	@AfterMethod
	public void tearDown() 
	{		
		//driver.quit();
	}
	

}
