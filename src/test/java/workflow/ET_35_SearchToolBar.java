package workflow;

import org.testng.annotations.Test;

import pages.FolderPage;
import common.TestBase;
import common.Util;

public class ET_35_SearchToolBar extends TestBase{
	public static FolderPage folderPage;
	static final String folderName = Util.getRandomString(10); 
	public ET_35_SearchToolBar(){
		super();
	}
	@Test
	public void execute()
	{
		
		folderPage = pages.login().webEditorLogin().clickOnFiles();
		folderPage.selectMyFilesMenuItem();
		folderPage.getCreateFolderDialolg();
		folderPage.createFolder(folderName);
		folderPage.searchFolder(folderName);
		folderPage.clickonSearch();
		folderPage.matchFileName(folderName);
		
	}

}


