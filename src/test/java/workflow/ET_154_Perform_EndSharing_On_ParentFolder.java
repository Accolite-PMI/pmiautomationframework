package workflow;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import common.TestBase;
import common.Util;
import pages.FolderPage;
import pages.HomePage;
import pages.LoginPage;

public class ET_154_Perform_EndSharing_On_ParentFolder extends TestBase
{
	
	public static FolderPage fPage;
	public static LoginPage lPage;
	public static HomePage hPage;
	static final String folderName = Util.getRandomString(10); 
	static final String tempfolderName = Util.getRandomString(10);
	static final String subFolderName = Util.getRandomString(10);
	SoftAssert softAssert = new SoftAssert();
	
	public ET_154_Perform_EndSharing_On_ParentFolder() 
	{
		super();
	}
	
	@BeforeTest
	public void init() 
	{
		System.out.println("Initialization...");
		hPage = pages.login().webEditorLogin();	
	}
	
	@Test
	public void test() throws InterruptedException
	{
		System.out.println("Test......");
		fPage = hPage.clickOnFiles();		
		System.out.println("Selecting MyFiles...");
		Util.sleep(1000);
		fPage.selectMyFilesMenuItem();
		System.out.println("Uploading Files...");
		fPage.uploadFile();
		fPage.getCreateFolderDialolg();
		fPage.createFolder(folderName);
		fPage.createSubFolder(subFolderName, folderName);
		fPage.selectCreatedFolder(folderName);
		fPage.shareFolderToAnotherUser("mounika4");
		
		lPage = fPage.logoutCurrentUser();
		hPage = lPage.customUserLogin("mounika4@pmi.com", "Admin1234");
		fPage = hPage.clickOnFiles();
		fPage.selectSharedWithMenuItem();
		
		if(fPage.checkSharedFolder(folderName))
		{
			fPage.endSharing(folderName);
		}
		else
		{
			System.out.println("Failed to share the folder...");
		}
					
		Assert.assertEquals(fPage.checkSharedFolder(folderName), false);
				
	}

	@AfterTest
	public void tearDown() 
	{		
		System.out.println("TearDown");
		driver.quit();
	}
	
}
