package workflow;

import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import common.TestBase;
import pages.HomePage;

public class ET68_LandingPageTest extends TestBase{
	
	HomePage hPage;
	SoftAssert softAssert = new SoftAssert();
	
	public ET68_LandingPageTest() {		
		super();
	}
	
	//@BeforeMethod
	@BeforeTest
	public void init() 
	{
		System.out.println("Initialization...");
		hPage = pages.login().adminLogin();		
	}
	
	@Test
	public void verifyProtienMetricsIcon() 
	{
		System.out.println("Test1");
		boolean flag = hPage.checkProtienMetricsIcon();
		Assert.assertTrue(flag);		
	}
	
	@Test
	public void verifyConfigTab() 
	{
		System.out.println("Test2");
		boolean flag = hPage.checkConfigurationTab();
		Assert.assertTrue(flag);			
	}
	
	@Test
	public void verifyUsersTab() 
	{
		System.out.println("Test3");
		boolean flag = hPage.checkUsersTab();
		Assert.assertTrue(flag);			
	}
	
	@Test
	public void verifyGroupsTab() 
	{
		System.out.println("Test4");
		boolean flag = hPage.checkGroupsTab();
		Assert.assertTrue(flag);			
	}
	
	@Test
	public void verifyDeletedTab() 
	{
		System.out.println("Test5");
		boolean flag = hPage.checkDeletionsTab();
		Assert.assertTrue(flag);			
	}
	
	@AfterMethod
	public void tearDown() 
	{		
		System.out.println("TearDown");
		//driver.quit();
	}

}
