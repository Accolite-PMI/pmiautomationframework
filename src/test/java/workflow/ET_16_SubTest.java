package workflow;

import org.testng.annotations.Test;

import pages.FolderPage;
import pages.HomePage;
import pages.LoginPage;
import common.TestBase;
import common.Util;

public class ET_16_SubTest extends TestBase {
	
	public static FolderPage folderPage;
	public static LoginPage loginPage;
	public static HomePage homePage;
	static final String folderName = Util.getRandomString(10); 
	static final String tempfolderName = Util.getRandomString(10);
	static final String subFolderName = Util.getRandomString(10);
	
	public ET_16_SubTest(){
		super();
	}
	
	@Test
	public void test() throws Exception{
		folderPage = pages.login()
				.webEditorLogin()
				.clickOnFiles();
		folderPage.selectMyFilesMenuItem();
		folderPage.uploadFile();
		folderPage.getCreateFolderDialolg();
		folderPage.createFolder(folderName);
		folderPage.createSubFolder(subFolderName, folderName);
		folderPage.selectCreatedFolder(folderName);
		folderPage.shareFolderToAnotherUser("mounika4");
		
		folderPage.selectMyFilesMenuItem();
		folderPage.getCreateFolderDialolg();
		folderPage.createFolder(tempfolderName);
		folderPage.selectCreatedFolder(tempfolderName);
		folderPage.shareFolderToAnotherUser("mounika4");
		
		loginPage = folderPage.logoutCurrentUser();
		homePage = loginPage.customUserLogin("mounika4@pmi.com", "Admin1234");
		folderPage = homePage.clickOnFiles();
		//folderPage.selectSharedWithMenuItem();
		//folderPage.validateSharedFolder(folderName);
		folderPage.validateDisabledOption(folderName);
		folderPage.verifyMoveOption(folderName,subFolderName,tempfolderName);
		
		
		
	}

}
