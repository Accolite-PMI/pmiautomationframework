package workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import pages.FolderPage;
import pages.HomePage;
import pages.LoginPage;
import common.TestBase;
import common.Util;

public class ET_16_FolderActionsCheck_UserWith_CanShare_Permissions extends TestBase{
	public static FolderPage folderPage;
	public static LoginPage loginPage;
	public static HomePage homePage;
	static final String folderName = Util.getRandomString(10); 

	public ET_16_FolderActionsCheck_UserWith_CanShare_Permissions(){
		super();
	}



	@Test
	public void test() throws Exception{
		folderPage = pages.login()
				.webEditorLogin()
				.clickOnFiles();
		folderPage.selectMyFilesMenuItem();
		folderPage.getCreateFolderDialolg();
		folderPage.createFolder(folderName);
		folderPage.selectCreatedFolder(folderName);
		//folderPage.ClickNewDropDown();
		//folderPage.uploadFile();
		folderPage.shareFolderToAnotherUser("pankaj8");
		loginPage = folderPage.logoutCurrentUser();
		homePage = loginPage.customUserLogin("pankaj8@pmi.com", "007test!@");
		folderPage = homePage.clickOnFiles();
		folderPage.selectSharedWithMenuItem();
		folderPage.validateSharedFolder(folderName);
	}
	
	@AfterMethod
	public void tearDown(){
		//driver.quit();
	}

}
